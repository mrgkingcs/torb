#
# MIT License
#
# Copyright (c) 2024 Greg King
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included in all copies
# or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import sys
from pathlib import Path

from imageProcessor import ImageProcessor
from riffwriter import RiffWriter


# ===============================================
class Bitmap2TIM:
    """Controls the overall flow of the conversion."""

    def __init__(self):
        """Constructor for Bitmap2Tim.
        The instance is valid from the outset.
        """
        self.__outputFileName = None
        self.__inputFileNameList = []
        self.__maxPaletteEntries = 16

        self.__inputImages = []
        #self.__outputEncoder = None
        self.__palette = None

    def __log(self, message):
        """Logs a message for the user."""
        print(message)

    def __checkAbleToStart(self):
        """Checks if the settings are sufficient to start the process."""
        errorMessage = None
        if self.__outputFileName is None:
            errorMessage = "Cannot find input file"
        elif len(self.__inputFileNameList) == 0:
            errorMessage = "No input files specified"

        if errorMessage is not None:
            self.__log("ERROR - unable to start: " + errorMessage)

        return errorMessage is None

    def __openInputImages(self):
        """Opens the input image files"""
        for inputFileName in self.__inputFileNameList:
            processor = ImageProcessor(self.__maxPaletteEntries)
            if processor.load(inputFileName):
                self.__inputImages.append(processor)
            else:
                self.__log("WARNING: unable to load input file \'" + inputFileName + "\' \n"
                           + processor.getErrorMessage()
                           )
        return len(self.__inputImages) > 0

    def __buildPalette(self):
        """Builds the overall palette from the input files"""
        self.__palette = []
        for processor in self.__inputImages:
            nextPalette = processor.getDesiredPalette()
            for entry in nextPalette:
                if entry not in self.__palette:
                    self.__palette.append(entry)

        if len(self.__palette) > self.__maxPaletteEntries:
            message = "ERROR - too many colours in input images ({0}) limit = {1}";
            self.__log(message.format( len(self.__palette), self.__maxPaletteEntries))
            self.__palette = None

        return self.__palette is not None

    def __compileImages(self):
        """Tells all the input image processors to compile to the new palette"""
        numSuccesses = 0

        for processor in self.__inputImages:
            if processor.compile(self.__palette):
                numSuccesses += 1
            else:
                message = "ERROR - failed to compile image {0}: {1}"
                self.__log(message.format(processor.getFilename(), processor.getErrorMessage()))

        return numSuccesses == len(self.__inputImages)

    def __encodePalette(self):
        """Encodes the list of 0-255 tuples in self.__palette
        into a RGBA5551 bytestream (LSB first)
        """
        result = bytearray()

        for entry in self.__palette:
            r = entry[0] >> 3
            g = entry[1] >> 3
            b = entry[2] >> 3
            a = 0

            word = r + (g << 5) + (b << 10) + (a << 15)
            result += word.to_bytes(2, 'little')

        return result

    def __writeFile(self):
        """Outputs the processed data to a RIFF format file"""
        file = open(self.__outputFileName, "wb")

        writer = RiffWriter(file)
        topLevelBlock = writer.startBlock("TIM0")

        # write the header block
        headerBlock = writer.startBlock("THD0")
        writer.writeData(len(self.__inputImages).to_bytes(1, 'little'))
        writer.writeData(bytearray([0, 0, 0]))
        writer.endBlock(headerBlock)

        # write the colour table / palette
        colourTableBlock = writer.startBlock("CTB0")
        writer.writeData(self.__encodePalette())
        writer.endBlock(colourTableBlock)

        # write each buffer/image
        for processor in self.__inputImages:
            bufferBlock = writer.startBlock("BUF0")

            # write the header block for this buffer
            bufferHeader = writer.startBlock("BHD0")
            writer.writeData(processor.getImageWidth().to_bytes(2, 'little'))
            writer.writeData(processor.getImageHeight().to_bytes(2, 'little'))
            writer.endBlock(bufferHeader)

            # write the compiled pixel block for this buffer
            pixelBlock = writer.startBlock("PXE0")
            writer.writeData(processor.getCompiledPixelBuffer())
            writer.endBlock(pixelBlock)

            writer.endBlock(bufferBlock)

        writer.endBlock(topLevelBlock)

        file.close()

        return True

    def setOutputFile(self, outputFileName):
        """Sets the filename to write the processed graphics to."""
        self.__outputFileName = outputFileName

    def addInputFile(self, inputFileName):
        """Adds a graphics file to load from."""
        self.__inputFileNameList.append(inputFileName)

    def run(self):
        """Kicks off the processing."""
        success = True

        # check we have enough info to actually run
        if success:
            success = self.__checkAbleToStart()

        # try to load all the input images
        if success:
            success = self.__openInputImages()

        # produce a bank-wide palette from the images
        if success:
            success = self.__buildPalette()

        # compile the word-streams for each image
        if success:
            success = self.__compileImages()

        # produce the output file
        if success:
            success = self.__writeFile()

        return success


# ===============================================
class ArgParser:
    """Parses command line arguments into a settings
    dictionary.

    Usage: bitmap2TIM outputFile.riff inputFile1.png inputFile2.png ...
    """

    def __init__(self, controller):
        """Constructor for the ArgParser
        In order to work, it needs a target controller
        """
        self.__controller = controller

    def parse(self, argsList):
        """Parse argsList and set up controller accordingly.
        Check here for the minimum args
        (outputFilename and one inputFilename)
        """

        result = True

        if len(argsList) < 3:
            print("Usage: bitmap2TIM outputFile.riff <inputFiles>")
            result = False
        else:
            self.__controller.setOutputFile(argsList[1])
            for fileName in argsList[2:]:
                self.__controller.addInputFile(fileName)

        return result


# ===============================================
if __name__ == "__main__":
    appResult = -1
    appCtrl = Bitmap2TIM()

    if ArgParser(appCtrl).parse(sys.argv):
        if appCtrl.run():
            appResult = 0

    exit(appResult)
