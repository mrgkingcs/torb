#
# MIT License
#
# Copyright (c) 2024 Greg King
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included in all copies
# or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
import math

from PIL import Image, UnidentifiedImageError
from srrencoder import SRREncoder


class ImageProcessor:
    def __init__(self, maxPaletteEntries):
        self.__maxPaletteEntries = maxPaletteEntries
        self.__filename = None
        self.__image = None
        self.__errorMessage = ""
        self.__desiredPalette = None
        self.__compiledPixBuff = None

    def __setError(self, message):
        self.__image = None
        self.__desiredPalette = None
        self.__errorMessage = message

    def __calcDesiredPalette(self):
        if self.__image is not None:
            if self.__image.getpalette() is None:
                self.__image = self.__image.quantize(self.__maxPaletteEntries)

            palette = self.__image.getpalette()
            if palette is not None:
                self.__desiredPalette = []
                for idx in range(0, len(palette), 3):
                    self.__desiredPalette.append((palette[idx], palette[idx+1], palette[idx+2]))
            else:
                self.__setError("Cannot produce palette")

    def __remapPalette(self, newPalette):
        newToOldMap = [0] * len(newPalette)
        for oldIdx, entry in self.__desiredPalette.enumerate():
            try:
                # try to find the exact match
                newIndex = self.__desiredPalette.index(entry)
            except ValueError:
                # if no exact match, find closest
                minDistance = 255 * 3
                closestNewIndex = -1
                for newIdx, newEntry in newPalette.enumerate():
                    compDist = [abs(newEntry[0] - entry[0]),
                                abs(newEntry[1] - entry[1]),
                                abs(newEntry[2] - entry[2])]
                    distance = math.sqrt(compDist[0] * compDist[0] +
                                         compDist[1] * compDist[1] +
                                         compDist[2] * compDist[2]
                                         )
                    if distance < minDistance:
                        minDistance = distance
                        closestNewIndex = newIdx
                newIndex = closestNewIndex

            newToOldMap[newIndex] = oldIdx

        self.__image = self.__image.remap_palette(newToOldMap)

    def __compilePixels(self):
        dimensions = (self.__image.width, self.__image.height)
        pixels = list(self.__image.getdata())
        self.__compiledPixBuff = SRREncoder().encode(dimensions, pixels)
        if self.__compiledPixBuff is None:
            self.__setError("Failed to compile pixels")

    def getFilename(self):
        return self.__filename

    def getErrorMessage(self):
        return self.__errorMessage

    def getDesiredPalette(self):
        return self.__desiredPalette

    def load(self, filename):
        self.__filename = filename
        try:
            self.__image = Image.open(filename)
        except FileNotFoundError:
            self.__setError("File not found")
        except UnidentifiedImageError:
            self.__setError("Unidentified image format")

        self.__calcDesiredPalette()

        return self.__image is not None

    def getImageWidth(self):
        return self.__image.width

    def getImageHeight(self):
        return self.__image.height

    def getCompiledPixelBuffer(self):
        return self.__compiledPixBuff

    def compile(self, newPalette):
        if newPalette != self.__desiredPalette:
            self.__remapPalette(newPalette)

        self.__compilePixels()

        return self.__compiledPixBuff is not None
