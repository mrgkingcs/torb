#
# MIT License
#
# Copyright (c) 2024 Greg King
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included in all copies
# or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

class SRREncoder:
    """Class to encode the pixels from a PIL/Pillow Image
    into a 16-bit binary code stream.

    Skip block: top two bits = 00; lower 14 bits = skip count
    Repeat block: top two bits = 01; next 4 are 4-bit colour index; 10 bit repeat count
    Raw block: top two bits = 10; 14-bits pixel count
                (followed by sequence of 4-bit colour indices - padded to 2-byte boundary)

    The final command in a row MUST be a skip
    (so the runtime can adjust it to skip to next row in target buffer)
    If opaque pixels go right up to the end of a row, a skip-0-pixels command must be added
    """

    COMMAND_SIZE_BYTES = 2

    BLOCK_TYPE_MASK = 0xC000
    BLOCK_TYPE_SKIP = 0x0000
    BLOCK_TYPE_REPEAT = 0x4000
    BLOCK_TYPE_RAW = 0x8000

    REPEAT_COLOUR_SHIFT = 10

    class RepeatBlock:
        """Class to represent and encode a repeat block"""

        def __init__(self, colour, count):
            self.__colour = colour
            self.__count = count

        def getCombinedAsRaw(self, otherRepeatBlock):
            combinedPixels = [self.__colour] * self.__count
            combinedPixels += [otherRepeatBlock.__colour] * otherRepeatBlock.__count
            return SRREncoder.RawBlock(combinedPixels)

        @property
        def byteSize(self):
            return 2

        @property
        def count(self):
            return self.__count

        @property
        def colour(self):
            return self.__colour

        def encoded(self):
            command = SRREncoder.BLOCK_TYPE_REPEAT
            command += (self.__colour & 0xf) << SRREncoder.REPEAT_COLOUR_SHIFT
            command += self.__count & 0x3ff
            return command.to_bytes(2, 'little')

    class SkipBlock:
        """Class to represent and encode a skip block"""

        def __init__(self, count):
            self.__count = count

        @property
        def byteSize(self):
            return 2

        @property
        def count(self):
            return self.__count

        def encoded(self):
            command = SRREncoder.BLOCK_TYPE_SKIP
            command += self.__count & 0x3fff
            return command.to_bytes(2, 'little')

    class RawBlock:
        """Class to represent and encode a raw block"""

        def __init__(self, pxColourList=[]):
            self.__pxColourList = pxColourList

        def appendPixels(self, pxColourList):
            self.__pxColourList += list(pxColourList)

        @property
        def byteSize(self):
            """Calculate size of raw block = 2-byte-command + 2-byte-padded-colours"""

            # calc numbytes with real colours in (divide by 2, rounding up)
            numColourBytes = int((len(self.__pxColourList) / 2) + 0.5)

            # then pad to a multiple of two bytes
            numColourBytes += numColourBytes & 1

            return SRREncoder.COMMAND_SIZE_BYTES + numColourBytes

        @property
        def count(self):
            return len(self.__pxColourList)

        def getCombinedWith(self, pxColourList):
            return SRREncoder.RawBlock(self.__pxColourList + list(pxColourList))

        def encoded(self):
            result = bytearray()
            command = SRREncoder.BLOCK_TYPE_RAW
            command += len(self.__pxColourList) & 0x3fff
            result += command.to_bytes(2, 'little')

            paddedColourList = self.__pxColourList[:]
            if len(paddedColourList) % 4:
                numToPad = 4 - (len(paddedColourList) % 4)
                paddedColourList += [0]*numToPad

            for idx in range(0, len(self.__pxColourList), 2):
                byteVal = (paddedColourList[idx] & 0xf) + ((paddedColourList[idx + 1] & 0xf) << 4)
                result.append(byteVal)

            return result

    def __init__(self):
        self.__encodedBlocks = None
        self.__pendingCmd = None
        self.__dimensions = None
        self.__pixels = None
        self.__currPxIndex = 0

    def __appendBytes(self, bytesToAppend):
        self.__encodedBlocks += bytesToAppend

    def __handleRepeatAfterRepeat(self, potRptCmd):
        # if single raw block would be the same size or
        # smaller than the two repeats...
        combinedSize = self.__pendingCmd.byteSize + potRptCmd.byteSize
        equivRaw = self.__pendingCmd.getCombinedAsRaw(potRptCmd)

        if equivRaw.byteSize <= combinedSize:
            # ...replace the pending repeat with a raw
            self.__pendingCmd = equivRaw
        else:
            # ...otherwise, put the pending repeat in the encoded buffer...
            self.__appendBytes(self.__pendingCmd.encoded())

            # ...and replace the pending repeat with the new one
            self.__pendingCmd = potRptCmd

        # either way, increment the current pixel index
        self.__currPxIndex += potRptCmd.count

    def __handleRepeatAfterRaw(self, potRptCmd):
        # if repeat folded into raw block would be the same or smaller
        # than the separate raw / repeat...
        repeatColours = [potRptCmd.colour] * potRptCmd.count
        combinedBlock = self.__pendingCmd.getCombinedWith(repeatColours)
        separateSize = self.__pendingCmd.byteSize + potRptCmd.byteSize

        if combinedBlock.byteSize <= separateSize:
            # ...append new pixels to pending raw block
            self.__pendingCmd = combinedBlock
        else:
            # ...write pending raw to buffer
            # and store repeat block as pending
            self.__appendBytes(self.__pendingCmd.encoded())
            self.__pendingCmd = potRptCmd

        self.__currPxIndex += potRptCmd.count

    def __encodeRow(self):
        rowBeyondEndIdx = self.__currPxIndex + self.__dimensions[0]

        self.__pendingCmd = None

        while self.__currPxIndex < rowBeyondEndIdx:
            repeatCount = 1
            while self.__currPxIndex + repeatCount < rowBeyondEndIdx and \
                    self.__pixels[self.__currPxIndex] == \
                    self.__pixels[self.__currPxIndex + repeatCount]:
                repeatCount += 1

            potRptCmd = SRREncoder.RepeatBlock(self.__pixels[self.__currPxIndex], repeatCount)

            if self.__pendingCmd is None:
                # We don't yet have a pending command, so put this one in there
                self.__pendingCmd = potRptCmd
                self.__currPxIndex += repeatCount

            elif isinstance(self.__pendingCmd, SRREncoder.RepeatBlock):
                self.__handleRepeatAfterRepeat(potRptCmd)

            elif isinstance(self.__pendingCmd, SRREncoder.RawBlock):
                self.__handleRepeatAfterRaw(potRptCmd)

        if self.__pendingCmd is not None:
            self.__appendBytes(self.__pendingCmd.encoded())
            if not isinstance(self.__pendingCmd, SRREncoder.SkipBlock):
                self.__appendBytes(SRREncoder.SkipBlock(0).encoded())
            self.__pendingCmd = None

    def encode(self, dimensions, pixels):
        self.__encodedBlocks = bytearray()
        self.__dimensions = dimensions
        self.__pixels = pixels[:]

        self.__currPxIndex = 0
        for _ in range(dimensions[1]):
            self.__encodeRow()

        return self.__encodedBlocks

    def printDecoded(self):
        byteIdx = 0
        while byteIdx < len(self.__encodedBlocks):
            command = self.__encodedBlocks[byteIdx] + (self.__encodedBlocks[byteIdx + 1] << 8)
            print(f"{command:04x}: ", end='')
            if (command & SRREncoder.BLOCK_TYPE_MASK) == SRREncoder.BLOCK_TYPE_REPEAT:
                colour = (command >> SRREncoder.REPEAT_COLOUR_SHIFT) & 0xf
                repeatCount = command & 0x3f
                print(f"RPT: {repeatCount} of colour {colour} ")
            elif (command & SRREncoder.BLOCK_TYPE_MASK) == SRREncoder.BLOCK_TYPE_SKIP:
                skipCount = command & 0x3fff
                print(f"SKP: {skipCount}")
            elif (command & SRREncoder.BLOCK_TYPE_MASK) == SRREncoder.BLOCK_TYPE_RAW:
                colours = []
                colourCount = command & 0x3fff
                numBytesLeft = int((colourCount / 2) + 0.5)
                while numBytesLeft > 0:
                    currByte = self.__encodedBlocks[byteIdx + 2]
                    colours.append(currByte & 0xf)
                    colours.append(currByte >> 4)
                    byteIdx += 1
                    numBytesLeft -= 1

                print(f"RAW: {colourCount}: {colours[:colourCount]}")

            byteIdx += 2


# ======================================================================================
#  TEST CODE FOLLOWS
# ======================================================================================

def printAsWord16(byteArrayToPrint):
    """simply prints a byte array as LSB-first 4-digit hex.
    """
    numBytes = int(len(byteArrayToPrint) / 2) * 2
    outputStr = ""
    for idx in range(0, numBytes, 2):
        value = byteArrayToPrint[idx] + (byteArrayToPrint[idx + 1] << 8)
        text = hex(value)[2:]
        while len(text) < 4:
            text = "0" + text
        outputStr += text + "h,"
    print(outputStr)


def bytearrayAsHex(byteArray):
    result = ""
    for currByte in byteArray:
        result += hex(currByte >> 4)[2:]
        result += hex(currByte & 0xf)[2:]
        result += ','
    return result


def testSolidColourRows():
    """Test encoding image of solid colour rows"""

    # generate test image with solid colour rows
    dims = (TEST_WIDTH, TEST_HEIGHT)
    solidRowPx = []
    for idx in range(TEST_HEIGHT):
        solidRowPx += [idx] * TEST_WIDTH

    # do the encode
    encodedPxs = SRREncoder().encode(dims, solidRowPx)

    # test the encode worked properly
    EXPECTED_NUM_BYTES = TEST_HEIGHT * (REPEAT_BLOCK_SIZE + SKIP_BLOCK_SIZE)
    assert len(encodedPxs) == EXPECTED_NUM_BYTES, \
        f"Expected {EXPECTED_NUM_BYTES} encoded bytes, but got {len(encodedPxs)}"

    # check all upper bytes of the repeat blocks are encoded correctly
    for idx, upperByte in enumerate(encodedPxs[1::4]):
        actualBlockType = (upperByte << 8) & SRREncoder.BLOCK_TYPE_MASK
        assert actualBlockType == SRREncoder.BLOCK_TYPE_REPEAT, \
            f"Expected block type {SRREncoder.BLOCK_TYPE_REPEAT} at byte {idx}" + \
            f" but got {actualBlockType}"

        expectedColour = idx
        actualColour = ((upperByte << 8) >> SRREncoder.REPEAT_COLOUR_SHIFT) & 0xf
        assert actualColour == expectedColour, \
            f"Expected colour {expectedColour} but got {actualColour}"

        expectedCount = TEST_WIDTH >> 8
        actualCount = upperByte & 0x03
        assert actualCount == expectedCount, \
            f"Expected count {expectedCount} but got {actualCount}"

    # check all lower bytes of the repeat blocks are encoded correctly
    for idx, lowerByte in enumerate(encodedPxs[0::4]):
        expectedCount = TEST_WIDTH & 0xff
        actualCount = lowerByte
        assert actualCount == expectedCount, \
            f"Expected count {expectedCount} but got {actualCount}"

    # check all the row-end skip blocks are encoded correctly
    for idx in range(2, len(encodedPxs), 4):
        actualWord = encodedPxs[idx] + (encodedPxs[idx + 1] << 8)
        expectedWord = SRREncoder.BLOCK_TYPE_SKIP
        assert actualWord == expectedWord, \
            f"Expected skip word ({expectedWord:04x}) but got {actualWord:04x}"

    print("testSolidColourRows - PASSED")


def testMultiColourRows():
    """test encoding with multi-coloured rows
    (each band at least 2px wide)"""

    # generate the test image
    dims = (TEST_WIDTH, TEST_HEIGHT)
    solidRowPx = []
    for idx in range(TEST_HEIGHT):
        solidRowPx += [idx] * 2
        solidRowPx += [idx + 8] * (TEST_WIDTH - 2)

    # do the encode
    encoder = SRREncoder()
    encodedPxs = encoder.encode(dims, solidRowPx)

    # test the encode worked properly
    # encoder.printDecoded()

    currIdx = 0
    for rowIdx in range(TEST_HEIGHT):
        # check first repeat block
        actualWord = encodedPxs[currIdx] + (encodedPxs[currIdx + 1] << 8)
        currIdx += 2

        expectedColour = rowIdx
        expectedCount = 2
        expectedWord = SRREncoder.BLOCK_TYPE_REPEAT + \
                       (expectedColour << SRREncoder.REPEAT_COLOUR_SHIFT) + \
                       expectedCount

        assert actualWord == expectedWord, \
            f"Expected repeat block {expectedWord: 02x} but got {actualWord: 02x}"

        # check second repeat block
        actualWord = encodedPxs[currIdx] + (encodedPxs[currIdx + 1] << 8)
        currIdx += 2

        expectedColour = rowIdx + 8
        expectedCount = 6
        expectedWord = SRREncoder.BLOCK_TYPE_REPEAT + \
                       (expectedColour << SRREncoder.REPEAT_COLOUR_SHIFT) + \
                       expectedCount

        assert actualWord == expectedWord, \
            f"Expected repeat block {expectedWord:02x} but got {actualWord:02x}"

        # check end-of-row skip block
        actualWord = encodedPxs[currIdx] + (encodedPxs[currIdx + 1] << 8)
        currIdx += 2

        expectedWord = SRREncoder.BLOCK_TYPE_SKIP

        assert actualWord == expectedWord, \
            f"Expected skip block {expectedWord:02x} but got {actualWord:02x}"

    print("testMultiColourRows - PASSED")


def testSolidColour1pxColumns():
    """test encoding with multi-coloured rows
    (each band at least 2px wide)"""

    # generate the test image
    dims = (TEST_WIDTH, TEST_HEIGHT)
    pxBuffer = []
    for idx in range(TEST_HEIGHT):
        for colIdx in range(TEST_WIDTH):
            pxBuffer.append(colIdx + 1)

    # do the encode
    encoder = SRREncoder()
    encodedPxs = encoder.encode(dims, pxBuffer)

    # test the encode worked properly
    #encoder.printDecoded()

    currIdx = 0
    for rowIdx in range(TEST_HEIGHT):
        # check raw block command
        actualWord = encodedPxs[currIdx] + (encodedPxs[currIdx + 1] << 8)
        currIdx += 2

        expectedPxCount = TEST_WIDTH
        expectedWord = SRREncoder.BLOCK_TYPE_RAW + expectedPxCount

        assert actualWord == expectedWord, \
            f"Expected raw block {expectedWord:02x} but got {actualWord:02x}"

        # check raw block colours
        expectedColours = bytearray([1 + (2 << 4), 3 + (4 << 4), 5 + (6 << 4), 7 + (8 << 4)])
        actualColours = bytearray(encodedPxs[currIdx:currIdx + int(TEST_WIDTH / 2)])
        currIdx += int(TEST_WIDTH / 2)
        assert actualColours == expectedColours, \
            f"Expected colours {bytearrayAsHex(expectedColours)} but got {bytearrayAsHex(actualColours)}"

        # check end-of-row skip block
        actualWord = encodedPxs[currIdx] + (encodedPxs[currIdx + 1] << 8)
        currIdx += 2

        expectedWord = SRREncoder.BLOCK_TYPE_SKIP

        assert actualWord == expectedWord, \
            f"Expected skip block {expectedWord:02x} but got {actualWord:02x}"

    print("testSolidColour1pxColumns - PASSED")


def testRawRepeatRawRows():
    """test encoding with 2px either side of a 4px repeat"""

    # generate the test image
    dims = (16, TEST_HEIGHT)
    pxBuffer = []
    for idx in range(TEST_HEIGHT):
        pxBuffer.append(1)
        pxBuffer.append(2)
        pxBuffer.append(3)
        for _ in range(10):
            pxBuffer.append(4)
        pxBuffer.append(5)
        pxBuffer.append(6)
        pxBuffer.append(7)

    # do the encode
    encoder = SRREncoder()
    encodedPxs = encoder.encode(dims, pxBuffer)

    # test the encode worked properly
    #encoder.printDecoded()

    currIdx = 0

    # check first raw block command
    actualWord = encodedPxs[currIdx] + (encodedPxs[currIdx + 1] << 8)
    currIdx += 2

    expectedPxCount = 3
    expectedWord = SRREncoder.BLOCK_TYPE_RAW + expectedPxCount

    assert actualWord == expectedWord, \
        f"Expected raw block {expectedWord:02x} but got {actualWord:02x}"

    # check first raw block colours
    expectedColours = bytearray([1 + (2 << 4), 3])
    numBytes = int((expectedPxCount / 2)+0.5)
    actualColours = bytearray(encodedPxs[currIdx:currIdx + numBytes])
    currIdx += numBytes
    assert actualColours == expectedColours, \
        f"Expected colours {bytearrayAsHex(expectedColours)} but got {bytearrayAsHex(actualColours)}"

    # check repeat block
    actualWord = encodedPxs[currIdx] + (encodedPxs[currIdx + 1] << 8)
    currIdx += 2

    expectedColour = 4
    expectedCount = 10
    expectedWord = SRREncoder.BLOCK_TYPE_REPEAT + \
                   (expectedColour << SRREncoder.REPEAT_COLOUR_SHIFT) + \
                   expectedCount

    assert actualWord == expectedWord, \
        f"Expected repeat block {expectedWord:02x} but got {actualWord:02x}"

    # check second raw block command
    actualWord = encodedPxs[currIdx] + (encodedPxs[currIdx + 1] << 8)
    currIdx += 2

    expectedPxCount = 3
    expectedWord = SRREncoder.BLOCK_TYPE_RAW + expectedPxCount

    assert actualWord == expectedWord, \
        f"Expected raw block {expectedWord:02x} but got {actualWord:02x}"

    # check first raw block colours
    expectedColours = bytearray([5 + (6 << 4), 7])
    numBytes = int((expectedPxCount / 2)+0.5)
    actualColours = bytearray(encodedPxs[currIdx:currIdx + numBytes])
    currIdx += numBytes
    assert actualColours == expectedColours, \
        f"Expected colours {bytearrayAsHex(expectedColours)} but got {bytearrayAsHex(actualColours)}"

    # check end-of-row skip block
    actualWord = encodedPxs[currIdx] + (encodedPxs[currIdx + 1] << 8)
    currIdx += 2

    expectedWord = SRREncoder.BLOCK_TYPE_SKIP

    assert actualWord == expectedWord, \
        f"Expected skip block {expectedWord:02x} but got {actualWord:02x}"

    print("testRawRepeatRawRows - PASSED")


if __name__ == "__main__":
    """Unit tests for encode()"""
    REPEAT_BLOCK_SIZE = 2
    SKIP_BLOCK_SIZE = 2

    TEST_WIDTH = 8
    TEST_HEIGHT = 8

    testSolidColourRows()
    testMultiColourRows()
    testSolidColour1pxColumns()
    testRawRepeatRawRows()
