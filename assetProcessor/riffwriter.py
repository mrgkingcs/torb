#
# MIT License
#
# Copyright (c) 2024 Greg King
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included in all copies
# or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import os


class RiffBlockError(Exception):
    def __init__(self, message):
        super().__init__(message)


class RiffWriter:
    """A class for writing a generic RIFF-format file"""

    BLOCK_HEADER_SIZE = 8

    def __init__(self, outBufferedIOBase):
        self.__outStream = outBufferedIOBase
        self.__blockLocationStack = []

    def startBlock(self, blockID):
        """Start a new block in the RIFF file"""
        currLocation = self.__outStream.tell()
        self.__blockLocationStack.append(currLocation)

        # validate blockID
        while len(blockID) < 4:
            blockID += " "
        blockIDBytes = blockID.encode('utf8')
        if len(blockIDBytes) > 4:
            blockIDBytes = blockIDBytes[:4]

        # write block header
        self.__outStream.write(blockIDBytes)
        self.__outStream.write(bytearray([0, 0, 0, 0]))

        return len(self.__blockLocationStack)

    def writeData(self, bytesData):
        """Writes the bytesData straight into the current block"""
        self.__outStream.write(bytesData)

    def endBlock(self, blockID):
        """Ends the current block.
        If the given block ID does not match the current block,
        a RiffBlockError will be raised.
        """
        if blockID != len(self.__blockLocationStack):
            raise RiffBlockError("Attempting to close incorrect block")

        currPos = self.__outStream.tell()

        # force alignment to 4-byte boundary
        misalignedAmount = currPos & 3
        if misalignedAmount:
            self.writeData(bytearray([0]*(4-misalignedAmount)))
        currPos = self.__outStream.tell()

        # calculate data size of block being closed
        blockHeaderPosition = self.__blockLocationStack.pop()
        blockDataSize = (currPos - blockHeaderPosition) - RiffWriter.BLOCK_HEADER_SIZE

        # jump back to block header, write the data size and jump to the end again
        self.__outStream.seek(blockHeaderPosition+4, os.SEEK_SET)
        self.__outStream.write(blockDataSize.to_bytes(4, 'little'))
        self.__outStream.seek(0, os.SEEK_END)

