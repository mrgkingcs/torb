#!/usr/bin/sh
MSDOSUTILDIR=~/.msdos
MNTDIR=/mnt/msdos
PROJDIR=$MNTDIR/torb
DISTDIR=dist/Test/

# ensure msdos directory is mounted
$MSDOSUTILDIR/mountDrive.sh

# clean debug environment
rm -fr $PROJDIR

# copy new debug build to drive (include start.bat)
mkdir -p $PROJDIR
cp -ur $DISTDIR/* $PROJDIR
cp START.BAT $MNTDIR

echo "msdos drive contents:"
ls -l $MNTDIR
ls -l $PROJDIR

sync -f $MNTDIR/*

$MSDOSUTILDIR/startBochs.sh &

# put these in once gdb stub is sorted
sleep 4
seer --gdb-program /usr/bin/gdb-dos --connect /tmp/ptyDBG --sym $DISTDIR/torb.exe
