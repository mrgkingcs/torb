#!/usr/bin/sh
MSDOSUTILDIR=/home/greggo/.msdos
MNTDIR=/mnt/msdos
PROJDIR=$MNTDIR/torb
DISTDIR=dist/Debug/

# ensure msdos directory is mounted
$MSDOSUTILDIR/mountDrive.sh

# clean debug environment
rm -fr $PROJDIR

# copy new debug build to drive (include start.bat)
mkdir -p $PROJDIR
cp -ur $DISTDIR/* $PROJDIR

echo "msdos drive contents:"
ls -l $MNTDIR
ls -l $PROJDIR

sync -f $MNTDIR/*

read -p "Press ENTER once remote program is running."

seer --gdb-program /usr/local/djgpp/bin/gdb --connect /dev/ttyS4 --sym $DISTDIR/torb.exe
