#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=DJGPP-i586-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug
CND_ARTIFACT_NAME_Debug=torb.exe
CND_ARTIFACT_PATH_Debug=dist/Debug/torb.exe
CND_PACKAGE_DIR_Debug=dist/Debug/DJGPP-i586-Linux/package
CND_PACKAGE_NAME_Debug=torb.tar
CND_PACKAGE_PATH_Debug=dist/Debug/DJGPP-i586-Linux/package/torb.tar
# Release configuration
CND_PLATFORM_Release=DJGPP-i586-Linux
CND_ARTIFACT_DIR_Release=dist/Release/DJGPP-i586-Linux
CND_ARTIFACT_NAME_Release=torb
CND_ARTIFACT_PATH_Release=dist/Release/DJGPP-i586-Linux/torb
CND_PACKAGE_DIR_Release=dist/Release/DJGPP-i586-Linux/package
CND_PACKAGE_NAME_Release=torb.tar
CND_PACKAGE_PATH_Release=dist/Release/DJGPP-i586-Linux/package/torb.tar
# Test configuration
CND_PLATFORM_Test=DJGPP-i586-Linux
CND_ARTIFACT_DIR_Test=dist/Test
CND_ARTIFACT_NAME_Test=torb.exe
CND_ARTIFACT_PATH_Test=dist/Test/torb.exe
CND_PACKAGE_DIR_Test=dist/Test/DJGPP-i586-Linux/package
CND_PACKAGE_NAME_Test=torb.tar
CND_PACKAGE_PATH_Test=dist/Test/DJGPP-i586-Linux/package/torb.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
