#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=i586-pc-msdosdjgpp-gcc
CCC=i586-pc-msdosdjgpp-g++
CXX=i586-pc-msdosdjgpp-g++
FC=gfortran
AS=nasm

# Macros
CND_PLATFORM=DJGPP-i586-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/gamesystems/resmanager.o \
	${OBJECTDIR}/src/gamesystems/scenemanager.o \
	${OBJECTDIR}/src/gamesystems/torbgame.o \
	${OBJECTDIR}/src/gfxmanager.o \
	${OBJECTDIR}/src/graphics/mode13hrenderer.o \
	${OBJECTDIR}/src/log.o \
	${OBJECTDIR}/src/main.o \
	${OBJECTDIR}/src/scenes/teaserscene.o \
	${OBJECTDIR}/src/testengine.o \
	${OBJECTDIR}/src/timemanager.o


# C Compiler Flags
CFLAGS=-O0

# CC Compiler Flags
CCFLAGS=-O0
CXXFLAGS=-O0

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=-f coff

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/torb.exe

${CND_DISTDIR}/${CND_CONF}/torb.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/torb.exe ${OBJECTFILES} ${LDLIBSOPTIONS}

: START.BAT
	@echo Copying START.BAT
	cp $@ ${CND_DISTDIR}

${OBJECTDIR}/src/gamesystems/resmanager.o: src/gamesystems/resmanager.cpp
	${MKDIR} -p ${OBJECTDIR}/src/gamesystems
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDEBUG=1 -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/gamesystems/resmanager.o src/gamesystems/resmanager.cpp

${OBJECTDIR}/src/gamesystems/scenemanager.o: src/gamesystems/scenemanager.cpp
	${MKDIR} -p ${OBJECTDIR}/src/gamesystems
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDEBUG=1 -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/gamesystems/scenemanager.o src/gamesystems/scenemanager.cpp

${OBJECTDIR}/src/gamesystems/torbgame.o: src/gamesystems/torbgame.cpp
	${MKDIR} -p ${OBJECTDIR}/src/gamesystems
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDEBUG=1 -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/gamesystems/torbgame.o src/gamesystems/torbgame.cpp

${OBJECTDIR}/src/gfxmanager.o: src/gfxmanager.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDEBUG=1 -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/gfxmanager.o src/gfxmanager.cpp

${OBJECTDIR}/src/graphics/mode13hrenderer.o: src/graphics/mode13hrenderer.cpp
	${MKDIR} -p ${OBJECTDIR}/src/graphics
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDEBUG=1 -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/graphics/mode13hrenderer.o src/graphics/mode13hrenderer.cpp

${OBJECTDIR}/src/log.o: src/log.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDEBUG=1 -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/log.o src/log.cpp

${OBJECTDIR}/src/main.o: src/main.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDEBUG=1 -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/main.o src/main.cpp

${OBJECTDIR}/src/scenes/teaserscene.o: src/scenes/teaserscene.cpp
	${MKDIR} -p ${OBJECTDIR}/src/scenes
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDEBUG=1 -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/scenes/teaserscene.o src/scenes/teaserscene.cpp

${OBJECTDIR}/src/testengine.o: src/testengine.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDEBUG=1 -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/testengine.o src/testengine.cpp

${OBJECTDIR}/src/timemanager.o: src/timemanager.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDEBUG=1 -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/timemanager.o src/timemanager.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} 

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
