/*
 * MIT License
 * 
 * Copyright (c) 2024 Greg King
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifdef TEST

#include "testengine.h"

#include <cstdio>

using namespace std;



//======================================================================
// Actual test engine code
//======================================================================

TestRunner* TestRunner::instance = NULL;

void TestRunner::registerFixture(FixtureRunner* fixture) {
	fixture->next = NULL;
	if(fixtureHead == NULL) {
		fixtureHead = fixtureTail = fixture;
	} else {
		fixtureTail->next = fixture;
		fixtureTail = fixture;
	}
}

void TestRunner::runTests() {
	printf("============================================================\n");
	printf("Running tests...\n");
	FixtureRunner* currFixture = fixtureHead;	
	while(currFixture != NULL) {
		currFixture->runTests();
		currFixture = currFixture->next;
	}
	printf( "Done running tests\n");
	printf( "============================================================\n");
	
	printf( "\n\n==== RESULTS ====\n\n" );
	
	currFixture = fixtureHead;
	
	while(currFixture != NULL) {
		currFixture->printResults();
		currFixture = currFixture->next;
	}

	printf( "\n==== END RESULTS ====\n\n" );

}

TestCase::TestCase() {
	next = NULL;
	passed = false;
}

FixtureRunner::FixtureRunner() {
	next = NULL; 
}

void FixtureRunner::setName(const char* const _name) { 	
	name = _name; 
}

const char* const FixtureRunner::getName() {
	return name;
}

void FixtureRunner::runTests() {
	printf( "\tFixture: %s\n", getName() );
	TestCase* currTest = head;
	while(currTest != NULL) {
		currTest->runTest();
		currTest = currTest->next;
	}
}

void FixtureRunner::printResults() {
	TestCase* currTest = head;
	int numPasses = 0;
	int numFailures = 0;
	
	while(currTest != NULL) {
		if(currTest->passed) {
			numPasses++;
		} else {
			if(numFailures == 0) {
				printf( "%s...\n", getName());
			}
			numFailures++;
			printf( "\t%s:%s - FAIL\n", getName(), currTest->getName());
		}
		currTest = currTest->next;
	}
	
	if(numFailures == 0) {
		if(numPasses == 0) {
			printf( "%s: No tests!!!\n", getName());
		} else if(numPasses == 1) {
			printf( "%s: single test passes\n", getName());
		} else {
			printf( "%s: all %d tests pass.\n", getName(), numPasses);
		}
	} else {
		printf("%s: %d of %d tests pass.\n", getName(), numPasses, (numPasses+numFailures));
	}
}



//======================================================================
// test cases for the test runner
//======================================================================

#define WHO_TESTS_THE_TESTERS 0
#if WHO_TESTS_THE_TESTERS
BEGIN_FIXTURE(TestTestRunner1)
	BEGIN_TEST(TestPass)
		CHECK(true, "Test a test can pass");
	END_TEST(TestPass)

	BEGIN_TEST(TestFail)
		CHECK(false, "Test a test can fail");
	END_TEST(TestFail)
END_FIXTURE(TestTestRunner1)

BEGIN_FIXTURE(TestTestRunner2)
	BEGIN_TEST(TestPass)
		CHECK(true, "Test a test can pass in a second fixture");
	END_TEST(TestPass)

	BEGIN_TEST(TestFail)
		CHECK(false, "Test a test can fail in a second fixture");
	END_TEST(TestFail)
END_FIXTURE(TestTestRunner2)

BEGIN_FIXTURE(TestTestRunner3)
	BEGIN_TEST(TestPass)
		CHECK(true, "Test a test can pass in a second fixture");
	END_TEST(TestPass)
END_FIXTURE(TestTestRunner3)

BEGIN_FIXTURE(TestTestRunner4)
END_FIXTURE(TestTestRunner4)
#endif

//======================================================================
// test main -> to run the tests
//======================================================================

void runTests(void) {
	TestRunner::getInstance()->runTests();

	printf("\nPress ENTER to exit.\n");

	getchar();
}

#endif