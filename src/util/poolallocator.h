/*
  MIT License
  
  Copyright (c) 2024 Greg King
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this 
  software and associated documentation files (the "Software"), to deal in the Software 
  without restriction, including without limitation the rights to use, copy, modify, 
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
  permit persons to whom the Software is furnished to do so, subject to the following 
  conditions:
  
  The above copyright notice and this permission notice shall be included in all copies 
  or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef POOLALLOCATOR_H
#define POOLALLOCATOR_H

#include "../core.h"

class PoolAllocator {
	uint8_t* m_Pool;
	void* m_FirstFree;
	uint32_t m_NumSlots;
	size_t m_SlotSize;
	
//	void* getSlotPtr(uint32_t slotIdx) {
//		void* result = NULL;
//		if (slotIdx < m_NumSlots) {
//			result = (void*)(m_Pool + (slotIdx*m_SlotSize));
//		}
//		return result;
//	}
	
public:
	PoolAllocator(size_t slotSize, uint32_t numSlots);
	virtual ~PoolAllocator();
	
	void* claim();
	void release(void* slotPtr);
	
	bool isValidSlot(void* slotPtr);
};

template <class T>
class TypedPoolAllocator {
private:
	PoolAllocator m_Allocator;
	
public:
	TypedPoolAllocator<T>(uint32_t numSlots) : m_Allocator(sizeof(T), numSlots) {
	}
	virtual ~TypedPoolAllocator() {}
	
	T* claim() {
		void* slotPtr = m_Allocator.claim();
		if(slotPtr != NULL) {
			MM_NEW (slotPtr) T();
		}
		return (T*)slotPtr;
	}
	
	void release(T* instance) {
		instance->~T();
		m_Allocator.release(instance);
	}
};


#endif /* POOLALLOCATOR_H */

