/*
 * MIT License
 * 
 * Copyright (c) 2024 Greg King
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "poolallocator.h"

#include <cstring>
using namespace std;

PoolAllocator::PoolAllocator(size_t slotSize, uint32_t numSlots) {
	m_SlotSize = (slotSize < sizeof(void*)) ? sizeof(void*) : slotSize;
	m_Pool = MM_NEW uint8_t[slotSize*numSlots];
	m_NumSlots = 0;
	m_FirstFree = m_Pool;

	if (m_Pool) {
		printf("Pool ptr: %p\n", m_Pool);
		m_NumSlots = numSlots;
		
		// loop through all slots (except the final one),
		// filling each slot with a pointer to the next slot
		// (to form the 'free slots list')
		uint8_t* slotPtr = m_Pool;
		for(uint32_t slotIdx = 0; slotIdx < m_NumSlots-1; slotIdx++, slotPtr += m_SlotSize) {
			uint8_t* nextPtr = slotPtr + m_SlotSize;
			void** slotPtrAsVoidPtrPtr = (void**)slotPtr;
			*slotPtrAsVoidPtrPtr = (void*)nextPtr;			
		}

		// set the final slot/free block ptr to NULL
		void** slotPtrAsVoidPtrPtr = (void**)slotPtr;
		*slotPtrAsVoidPtrPtr = NULL;
	}
}

PoolAllocator::~PoolAllocator() {
	if(m_Pool) {
		MM_DELETE [] m_Pool;
		m_Pool = NULL;
	}
}

void* PoolAllocator::claim() {
	void* result = m_FirstFree;
	if(m_FirstFree) {
		void* nextFree = *((void**)m_FirstFree);
		m_FirstFree = nextFree;

		memset(result, 0, m_SlotSize);
	}
	return result;
}

void PoolAllocator::release(void* slotPtr) {
	if (isValidSlot(slotPtr)) {
		printf("Freeing %p\n", slotPtr);
		void** slotPtrAsVoidPtrPtr = (void**)slotPtr;
		printf("Contents of slotPtr: %p\n", *slotPtrAsVoidPtrPtr);
		
		// check slot contents is not already part of
		// free-block list
		bool slotIsAlreadyFreed = false;
		
		// the slot can contain NULL and still be on the free-block list if it
		// is the only free slot
		slotIsAlreadyFreed |= (m_FirstFree == slotPtr);
		
		// if the slot contains a valid slot pointer, then it has already been freed
		slotIsAlreadyFreed |= isValidSlot(*slotPtrAsVoidPtrPtr);

		if(!slotIsAlreadyFreed) {
			*slotPtrAsVoidPtrPtr = m_FirstFree;
			m_FirstFree = slotPtr;
		}
	}
}

bool PoolAllocator::isValidSlot(void* slotPtr) {
	bool result = true;
	uint8_t* ptrForCheck = (uint8_t*)slotPtr;

	if (ptrForCheck < m_Pool) {
		result = false;
	} else if(ptrForCheck >= m_Pool + (m_NumSlots*m_SlotSize)) {
		result = false;
	} else if((ptrForCheck - m_Pool) % m_SlotSize != 0) {
		result = false;
	}
	return result;
}




#if TEST
#include "testengine.h"

BEGIN_FIXTURE(PoolAllocatorTests)
	BEGIN_TEST(ClaimAndRelease)
		const int TEST_SLOT_SIZE = 4;
		const int TEST_NUM_SLOTS = 4;
		PoolAllocator instance(TEST_SLOT_SIZE, TEST_NUM_SLOTS);

		void* firstClaim = instance.claim();
		printf("First claim: %p\n", firstClaim);
		CHECK(firstClaim != NULL, "First claim is not null");
		
		void* secondClaim = instance.claim();
		printf("Second claim: %p\n", secondClaim);
		CHECK(secondClaim != NULL && secondClaim != firstClaim,
				"Second claim is not null and is different to first."
				);
		
		instance.release(firstClaim);
		void* thirdClaim = instance.claim();
		CHECK(thirdClaim == firstClaim, "After releasing a slot, it is reused");
	END_TEST(ClaimAndRelease)
			
	BEGIN_TEST(OverAllocate)
		const int TEST_SLOT_SIZE = 8;
		const int TEST_NUM_SLOTS = 4;
		PoolAllocator instance(TEST_SLOT_SIZE, TEST_NUM_SLOTS);
		
		void* claims[TEST_NUM_SLOTS];

		bool allClaimsSucceeded = true;
		for(int count = 0; count < TEST_NUM_SLOTS; count++) {
			claims[count] = instance.claim();
			allClaimsSucceeded &= (bool)(claims[count]);
		}
		CHECK(allClaimsSucceeded, "Claiming all slots succeeds");
		
		void* extraClaim = instance.claim();
		CHECK(extraClaim == NULL, "Claiming one more slot fails.");
		
		instance.release(claims[0]);
		extraClaim = instance.claim();
		CHECK(extraClaim != NULL, "After releasing a slot, the next claim works");
	END_TEST(OverAllocate)

	BEGIN_TEST(DoubleFree)
		const int TEST_SLOT_SIZE = 8;
		const int TEST_NUM_SLOTS = 2;
		PoolAllocator instance(TEST_SLOT_SIZE, TEST_NUM_SLOTS);
		
		void* firstClaim = instance.claim();
		void* secondClaim = instance.claim();
		
		instance.release(firstClaim);
		instance.release(firstClaim);
		void* thirdClaim = instance.claim();
		CHECK(thirdClaim, "After double-free, it is possible to claim again");
		
		void* fourthClaim = instance.claim();
		CHECK(fourthClaim == NULL, "Double-free does not free up two slots");
	END_TEST(DoubleFree)
			
END_FIXTURE(PoolAllocatorTests)

#endif