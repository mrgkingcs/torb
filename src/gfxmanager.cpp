/*
 * MIT License
 * 
 * Copyright (c) 2024 Greg King
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "gfxmanager.h"

#include <cstring>
#include <dpmi.h>

using namespace std;

#include "core.h"
#include "graphics/gfxrenderer.h"
#include "graphics/mode13hrenderer.h"


GfxManager::GfxManager() {
	m_BackBufferRenderer = NULL;
}

GfxManager::~GfxManager() {
	destroyRenderer();
}

void GfxManager::destroyRenderer() {
	if(m_BackBufferRenderer) {
		MM_DELETE [] m_BackBufferRenderer;
		m_BackBufferRenderer = NULL;
	}
}

void GfxManager::initMode13h() {
	destroyRenderer();
	m_BackBufferRenderer = MM_NEW Mode13hRenderer();
	if (m_BackBufferRenderer) {
		// set video mode
		__dpmi_regs regs;
		regs.x.ax = 0x13;
		__dpmi_int(0x10, &regs);
	}
}

void GfxManager::initMode3h() {
	destroyRenderer();
	
	// set video mode
	__dpmi_regs regs;
	regs.x.ax = 0x3;
	__dpmi_int(0x10, &regs);	
}

void GfxManager::flip() {
	if(m_BackBufferRenderer) {
		dosmemput(	m_BackBufferRenderer->getBuffer(), 
					m_BackBufferRenderer->getBufferByteSize(), 
					0xA0000
				);
	}
}

void GfxManager::fadeToBlack(uint32_t durationMillis, bool chain) {
	
}

// If fadeToBlack is still happening, this will _queue_ a buffer-flip
// and fade-in. (the buffer-flip and fade-in
// If fadeToBlack has finished, it will immediately 
void GfxManager::flipFadeFromBlack(uint32_t durationMillis, bool chain) {
	
}
	
bool GfxManager::isFading() {
	return false;
}



