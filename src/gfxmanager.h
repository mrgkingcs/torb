/*
  MIT License
  
  Copyright (c) 2024 Greg King
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this 
  software and associated documentation files (the "Software"), to deal in the Software 
  without restriction, including without limitation the rights to use, copy, modify, 
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
  permit persons to whom the Software is furnished to do so, subject to the following 
  conditions:
  
  The above copyright notice and this permission notice shall be included in all copies 
  or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef GFXMANAGER_H
#define GFXMANAGER_H

#include "core.h"

class GfxRenderer;

class GfxManager {
private:
	GfxRenderer* m_BackBufferRenderer;

	void destroyRenderer();
	
public:
	GfxManager();
	
	virtual ~GfxManager();
	
	//==========================================================================
	// Game-level interface
	//==========================================================================
	
	// set 320x200x8 video mode
	void initMode13h();
	
	// set normal text mode
	void initMode3h();
	
	//==========================================================================
	// SceneManager-level interface
	//==========================================================================
	
	// flip the backbuffer to the front
	void flip();
	
	// start an interrupt-driven palette fade to black
	void fadeToBlack(uint32_t durationMillis, bool chain = false);
	
	// start (or queue) an interrupt-driven palette fade to the normal palette
	void flipFadeFromBlack(uint32_t durationMillis, bool chain = false);
	
	// returns true if an interrupt-driven palette fade is in progress,
	// returns false otherwise
	bool isFading();

	// returns the main renderer
	GfxRenderer* getRenderer() { return m_BackBufferRenderer; }
};

#endif /* GFXMANAGER_H */

