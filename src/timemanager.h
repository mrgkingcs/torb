/*
  MIT License
  
  Copyright (c) 2024 Greg King
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this 
  software and associated documentation files (the "Software"), to deal in the Software 
  without restriction, including without limitation the rights to use, copy, modify, 
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
  permit persons to whom the Software is furnished to do so, subject to the following 
  conditions:
  
  The above copyright notice and this permission notice shall be included in all copies 
  or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef TIMEMANAGER_H
#define TIMEMANAGER_H

/*
 * TimeManager sets the PIT to as close to 44100Hz as possible for timing
 * VGM music files.  This should be more than precise enough for game timing.
 * 
 * Base timer frequency = 1193182Hz
 * Desired frequency	= 44100Hz
 * Freq. divider		= 27
 * 
 * However, 27 is too low, and causes crashes.  But!  108 works!
 * So actual frequency	= 11025Hz
 * 
 * TICKS TO MILLIS CONVERSION
 * ticks per millisecond = 1000 / 11025
 * 2^16 / 11.025 ~= 5944
 * so millis given by: (ticks * 5944) >> 16
 */

#include <dpmi.h>
#include <go32.h>
#include <pc.h>

#include "core.h"

// Doing time calculation above with divider so slower machines don't hang
#define TIME_RAW_FREQ (1193182)
#define TIME_IDEAL_FREQ (41000)
#define TIME_REALISTIC_DIVIDER (6)
#define TIME_ACTUAL_FREQ (TIME_IDEAL_FREQ / TIME_REALISTIC_DIVIDER)

#define TIME_FREQ_DIVIDER (TIME_RAW_FREQ / TIME_ACTUAL_FREQ)
#define TIME_TICK_CONV_FACTOR (((1<<16) * 1000) / TIME_ACTUAL_FREQ)

#define TICKS_TO_MILLIS(uint64_ticks)	( ((uint64_ticks)*TIME_TICK_CONV_FACTOR) >> 16 )
#define MILLIS_TO_TICKS(uint32_millis)	( ((uint32_millis) * TIME_ACTUAL_FREQ)) / 1000 )

typedef void(*TickCallback)();

class TimeManager {
	static const int MAX_CALLBACKS = 4;
	
	// 32-bits should allow many hours before wrap-around
	static uint32_t s_TickCount;

	// tick interrupt handler gubbins
	static _go32_dpmi_seginfo old_tick_handler;
	static _go32_dpmi_seginfo tick_handler;
	
	static TickCallback s_CallbackArr[MAX_CALLBACKS];
	static int s_NumCallbacks;
	
private:
	static inline void delay() {
//		__asm__ __volatile__ (
//				"push %%ecx	\n\t"
//				"mov %%ecx, 0x10\n\t"
//			"DELAY:\n\t"
//				"dec %%ecx\n\t"
//				"jnz DELAY\n\t"
//				"pop %%ecx"
//				:
//				:
//				:
//		);
	}
	
	static void tickHandler();
	
public:
	// start the timing system
	static void start();
	
	// stop the timing system
	static void stop();

	// add a callback to be executed every tick
	static void addTickCallback(TickCallback callback);
	
	// get the number of ticks since TimeManager::start()
	static uint32_t getTicks() { return s_TickCount; }
	
	// get the number of milliseconds since TimeManager::start()
	static uint32_t getMillis() { 
		uint64_t workingTickCount = s_TickCount;
		uint32_t millisCount = TICKS_TO_MILLIS(workingTickCount);
		return millisCount; 
	}
};

#endif /* TIMEMANAGER_H */

