/*
  MIT License
  
  Copyright (c) 2024 Greg King
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this 
  software and associated documentation files (the "Software"), to deal in the Software 
  without restriction, including without limitation the rights to use, copy, modify, 
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
  permit persons to whom the Software is furnished to do so, subject to the following 
  conditions:
  
  The above copyright notice and this permission notice shall be included in all copies 
  or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef LOG_H
#define LOG_H

#include <cstdarg>
#include <cstdio>
using namespace std;

#include "core.h"

#define LOGGING_ENABLED 1

#if LOGGING_ENABLED
	#define INFO(...)	\
		Logger::instance()->log(Logger::INFO, __FILE__, __LINE__, __VA_ARGS__);

	#define WARNING(...)	\
		Logger::instance()->log(Logger::WARNING, __FILE__, __LINE__, __VA_ARGS__);

	#define ERROR(...)	\
		Logger::instance()->log(Logger::ERROR, __FILE__, __LINE__, __VA_ARGS__);
#else
	#define INFO(...)

	#define WARNING(...)

	#define ERROR(...)
#endif	

class Logger {
public:
	enum LogLevel {
		NUM_LEVELS = 3,
		INFO = 3,
		WARNING = 2,
		ERROR = 1,
		NONE = 0
	};
	
	enum LogType {
		TO_FILE = 1,
		TO_CONSOLE = 2
	};
	
private:
	static Logger s_Logger;
	static const char* s_LogLevelPrefixList[];
	
	LogLevel m_LogLevel;
	FILE* m_LogFile;
	
	
	void reset(Logger::LogType logType, Logger::LogLevel logLevel);
	void ensureLogFileClosed();

public:
	static Logger* instance() {
		return &s_Logger;
	}
	
	static void start(LogType logType, LogLevel logLevel=INFO);
	static void stop();

	Logger();
	virtual ~Logger();
	
	void log(uint32_t level, const char* filename, int line, const char* format, ...);
};

#endif /* LOG_H */

