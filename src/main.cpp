/*
 * MIT License
 * 
 * Copyright (c) 2024 Greg King
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <go32.h>
#include <signal.h>

#include "core.h"
#include "gfxmanager.h"
#include "timemanager.h"
#include "gamesystems/torbgame.h"

#if DEBUG
#define GDB_IMPLEMENTATION
#include "gdbstub.h"
#endif

//------------------------------------------------------------------------------
//	Core interrupt setup/teardown
//------------------------------------------------------------------------------
static bool sigint_occurred = false;

static void sigint_handler(int exception_number) {
	sigint_occurred = true;
}

void initInterrupts() {
	// init sigint handler for clean exit
	_go32_dpmi_lock_data(&sigint_occurred, sizeof(sigint_occurred));
	_go32_dpmi_lock_code((void*)sigint_handler, 4096);
	signal(SIGINT, sigint_handler);
	
	INFO("Starting timer...");
	TimeManager::start();

#if DEBUG
	INFO("Setting up gdb...");
	gdb_start();
	INFO("Adding gdb_tick_handler as TickCallback...");
	TimeManager::addTickCallback(&gdb_tick_handler);
	printf("Waiting for debugger...\n"); 
	gdb_wait();
#endif
}

void cleanupInterrupts() {
	INFO("Stopping TimeManager...");
	TimeManager::stop();
	
	INFO("Exiting...");
	signal(SIGINT, SIG_DFL);
}


//------------------------------------------------------------------------------
//	Actually running the game
//------------------------------------------------------------------------------
void doGame() {
	INFO("Setting up game...");
	TorbGame game;

	game.setup();
	
	if (!game.getError()) {
		INFO("Entering main game loop...");
		uint32_t currMillis = TimeManager::getMillis();
		while(!game.getShouldQuit() && !sigint_occurred) {
			uint32_t frameMillis = TimeManager::getMillis() - currMillis;
			game.tick(frameMillis);
			currMillis += frameMillis;
		}
	} else {
		ERROR("Game setup failed with code %u.", game.getError());
	}
	
	INFO("Cleaning up game...");
	game.teardown();
}


//------------------------------------------------------------------------------
//	The big shiz
//------------------------------------------------------------------------------
#ifdef TEST
#include "testengine.h"
int main(void) {
	Logger::start(Logger::TO_FILE);
	initInterrupts();
	runTests();
	cleanupInterrupts();
	Logger::stop();
	return 0;
}
#else
int main(void) {
	Logger::start(Logger::TO_FILE);

	initInterrupts();
	
	doGame();

	cleanupInterrupts();

	Logger::stop();

	return 0;
}
#endif

