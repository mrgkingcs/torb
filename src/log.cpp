/*
 * MIT License
 * 
 * Copyright (c) 2024 Greg King
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "log.h"

#include <ctime>

Logger Logger::s_Logger;

const char* Logger::s_LogLevelPrefixList[] = {
	"",
	"ERROR",
	"WARNING",
	"INFO"
};

Logger::Logger() {
	m_LogLevel = NONE;
	m_LogFile = NULL;
}

Logger::~Logger() {
	ensureLogFileClosed();
}

//------------------------------------------------------------------------------
//	Makes sure any open log file is closed
//------------------------------------------------------------------------------
void Logger::ensureLogFileClosed() {
	if(m_LogFile) {
		fclose(m_LogFile);
		m_LogFile = NULL;
	}
}

//------------------------------------------------------------------------------
//	Resets the state of the logger, closing and re-opening the log file
//	if necessary
//------------------------------------------------------------------------------
void Logger::reset(Logger::LogType logType, Logger::LogLevel logLevel) {
	m_LogLevel = logLevel;

	time_t rawTime;
	struct tm * timeinfo;
	time(&rawTime);
	timeinfo = localtime(&rawTime);

	if(logType == TO_FILE) {
		const int MAX_FILENAME_LENGTH = 8+1+3;
		char filename[MAX_FILENAME_LENGTH+1];
		snprintf(filename, 
				 MAX_FILENAME_LENGTH, 
				 "%02u%02u%02u.log", 
				 timeinfo->tm_hour, 
				 timeinfo->tm_min, 
				 timeinfo->tm_sec
				);
		m_LogFile = fopen(filename, "w");
	} else {
		ensureLogFileClosed();
	}
	
	INFO("Starting logging at time %s", asctime(timeinfo));	
}

//------------------------------------------------------------------------------
//	Logs a message (or filters it out)
//------------------------------------------------------------------------------
void Logger::log(uint32_t level, const char* filename, int line, const char* format, ...) {
	va_list args;
	va_start(args, format);

	if (level <= m_LogLevel) {
		if (m_LogFile) {
			fprintf(m_LogFile,
					"%s: %s:%d - ", 
					s_LogLevelPrefixList[level],
					filename,
					line
				);
			vfprintf(m_LogFile, format, args);
			fprintf(m_LogFile, "\n");
			fflush(m_LogFile);
		} else {
			printf("%s: %s:%d - ", 
					s_LogLevelPrefixList[level],
					filename,
					line
				);
			vprintf(format, args);
			printf("\n");
		}
	}

	va_end(args);
}

//------------------------------------------------------------------------------
//	Starts the logging process
//------------------------------------------------------------------------------
void Logger::start(Logger::LogType logType, Logger::LogLevel logLevel) {
	instance()->reset(logType, logLevel);
}

//------------------------------------------------------------------------------
//	Stops the logging process
//------------------------------------------------------------------------------
void Logger::stop() {
	instance()->reset(TO_CONSOLE, NONE);
}
