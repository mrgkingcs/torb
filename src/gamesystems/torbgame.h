/*
  MIT License
  
  Copyright (c) 2024 Greg King
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this 
  software and associated documentation files (the "Software"), to deal in the Software 
  without restriction, including without limitation the rights to use, copy, modify, 
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
  permit persons to whom the Software is furnished to do so, subject to the following 
  conditions:
  
  The above copyright notice and this permission notice shall be included in all copies 
  or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef TORBGAME_H
#define TORBGAME_H

#include "core.h"

class GfxManager;
class ResManager;
class SceneManager;

class TorbGame {
	uint32_t m_Error;
	GfxManager* m_GfxMgr;
	ResManager* m_ResMgr;
	SceneManager* m_SceneMgr;
	
public:
	enum ErrorCode {
		ERROR_NONE = 0,
		ERROR_MEM_ALLOC_FAIL
	};
	
	TorbGame();
	virtual ~TorbGame();
	
	// sets up all the game's systems ready to start ticking
	void setup();
	
	// cleans up all the game's resources
	void teardown();
	
	// updates and renders the game
	void tick(uint32_t millis);

	// returns true if main game loop should exit
	bool getShouldQuit();
	
	// returns the current error code of the game
	uint32_t getError();
};

#endif /* TORBGAME_H */

