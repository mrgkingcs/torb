/*
  MIT License
  
  Copyright (c) 2024 Greg King
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this 
  software and associated documentation files (the "Software"), to deal in the Software 
  without restriction, including without limitation the rights to use, copy, modify, 
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
  permit persons to whom the Software is furnished to do so, subject to the following 
  conditions:
  
  The above copyright notice and this permission notice shall be included in all copies 
  or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef RESMANAGER_H
#define RESMANAGER_H

#include "core.h"

class Bitmap;

typedef void* RESOURCE_PTR;

class ResList {
private:
	static const int MAX_RES_LIST_ENTRIES = 32;
	struct ResListEntry {
		const char* resourcePath;
		RESOURCE_PTR* resourcePtrPtr;
	};
	struct ResListEntry m_Entries[MAX_RES_LIST_ENTRIES];
	uint16_t m_NumEntries;
public:
	ResList();
	virtual ~ResList();
	
	void clear();
	void addResource(RESOURCE_PTR* resourcePtrPtr, const char* const resourcePath);
};


//==============================================================================
// PathElementTable
//	A string table specialised for MS-DOS file/directory names 
//	(max length is 8.3 = 12 chars)
//	If a slot is empty, it is zeroed-out.
//	Slots are guaranteed to be null-terminated.
//==============================================================================
class PathElementTable {
private:
	static const int ENTRY_SIZE = 13;
	uint32_t m_MaxEntries;
	char* m_TableBuffer;
	char* m_InsertPtr;

public:
	static const uint32_t INVALID_INDEX = 0xffffffff;
	
	PathElementTable(uint32_t maxEntries);
	virtual ~PathElementTable();
	
	uint32_t addElement(const char* const elementStr);
	uint32_t findElement(const char* const elementStr) const;
	
	void removeElement(uint32_t index);
	const char* getElement(uint32_t index) const;
};


//==============================================================================
// ResourcePathParser
//	Given a const char* const, this class will extract path elements
//	(i.e. individual file/directory names) starting with the rightmost
//==============================================================================
class ResourcePathParser {
	static const int MAX_ELEMENT_LENGTH = 13;
	static const int MAX_PATH_LENGTH = MAX_ELEMENT_LENGTH * 16;
	char m_CurrentElement[MAX_ELEMENT_LENGTH];
	const char* m_ResourcePath;
	const char* m_ParsePtr;
	
public:
	ResourcePathParser(const char* const resourcePath);
	virtual ~ResourcePathParser();
	
	const char* const getCurrElement();
	void goToNextElement();
	
	bool isFinished() { return m_ParsePtr == m_ResourcePath; }
};

//==============================================================================
// ResManager
//	A class to manage allocation, loading and freeing of data from resource
//	files.
//==============================================================================

class ResManager {
private:
	static const int MAX_PATH_ELEMENT_ENTRIES = 256;
	static const int MAX_RESOURCES = 128;
	static const int MAX_SCENE_RESOURCE_IDS = MAX_RESOURCES*2;
	
	static const uint32_t INVALID_UUID = 0xffffffff;
	
	struct ResourceMapEntry {
		uint32_t UUID;
		void* resourcePtr;
		uint32_t refCount;
	};
		
	ResourceMapEntry m_ResourceMap[MAX_RESOURCES];
	uint32_t m_NumResources;
	
	PathElementTable* m_PathElementTable;
	
	uint32_t getOrCreateUUID(const char* const resourcePath);
	uint32_t getExistingUUID(const char* const resourcePath);
	
public:
	ResManager(const char* baseResourcePath);
	virtual ~ResManager();
	
	//==========================================================================
	// SceneManager-level interface
	//==========================================================================
	void setToRelease(const ResList* const resList);
	void setToLoad(const ResList* const resList);
	void setToKeep(const ResList* const resList);
	
	void doResourceChange();
	
	//==========================================================================
	// Scene-level interface
	//==========================================================================
	const Bitmap* getBitmap(uint16_t localID);
};

#endif /* RESMANAGER_H */

