/*
  MIT License
  
  Copyright (c) 2024 Greg King
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this 
  software and associated documentation files (the "Software"), to deal in the Software 
  without restriction, including without limitation the rights to use, copy, modify, 
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
  permit persons to whom the Software is furnished to do so, subject to the following 
  conditions:
  
  The above copyright notice and this permission notice shall be included in all copies 
  or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include "core.h"

class GfxManager;
class GfxRenderer;
class ResManager;
class ResList;
class SceneManager;

//==============================================================================
// The Scene class
//	A 'Scene' is a game section which requires resources to be loaded.
//	All game scenes should inherit from this and implement the methods so that
//	the SceneManager can perform nice scene transitions.
//==============================================================================
class Scene {
private:
	SceneManager* m_Manager;

public:
	Scene(SceneManager* manager) { 
		m_Manager = manager; 
	}
	
	SceneManager* getSceneManager() { return m_Manager; }
	
 	// Scenes must implement this to register any resources
	// they need for doPreloadRender()
	virtual void registerPreLoadResources(ResList* resList) const {}
	
	// Scenes must implement this to register any resources
	// they need during the main execution of the scene
	virtual void registerLoadResources(ResList* resList) const {}
	
	// Scenes must implement this to perform any required initialisation 
	// before the first call to update()
	virtual void start() {}
	
	// Scenes must implement this to perform any cleanup required
	// (Note: any registered resources will be freed automatically 
	//  _after_ this call - don't leave any dangling resource pointers!
	virtual void stop() {}

	// Once the pre-load resources are loaded, this method is called
	// to render into the back buffer ready for the flip & fade-in
	virtual void doPreloadRender(GfxRenderer* gfx, ResManager* res) {}
	
	// Called for the scene to do any state update
	virtual void update(uint32_t millis) {}
	
	// Called for the scene to do any drawing before the buffer flip
	virtual void render(GfxRenderer* gfx) {}
};

//==============================================================================
// The SceneManager class
//	A game should have one of these to manage resource loading/release and 
//	nice transitions between scenes
//==============================================================================
class SceneManager {
private:
	enum State {
		STATE_NONE,
		STATE_ERROR,
		STATE_SCENE_TRANSITION,
		STATE_WAIT_FOR_FADE_IN,
		STATE_SCENE_RUNNING
	};
	
	GfxManager* m_Gfx;
	ResManager* m_ResMgr;
	
	State m_State;
	Scene* m_CurrScene;
	Scene* m_NextScene;
	
	void doSceneTransition();
	
public:
	SceneManager(GfxManager* gfx, ResManager* resMgr);
	virtual ~SceneManager();
	
	// returns True if the outer game handler should quit the program
	// return False otherwise
	bool shouldQuit();

	// clears any scene stack and starts the given scene
	// NOTE: SceneManager will delete the scene instance!!
	void startScene(Scene* (*sceneCreate)(SceneManager*));
	
	// starts a game-exit scene transition
	void exit();

	// update the current scene
	void update(uint32_t millis);	
	
	// render the current scene and flip the graphics buffers
	void render();
};

#endif /* SCENEMANAGER_H */

