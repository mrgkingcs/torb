/*
 * MIT License
 * 
 * Copyright (c) 2024 Greg King
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "resmanager.h"

#include <cstring>
using namespace std;

#include "core.h"

//==============================================================================
// ResList methods
//==============================================================================
ResList::ResList() {
	m_NumEntries = 0;
}

ResList::~ResList() {
	
}

void ResList::clear() {
	m_NumEntries = 0;
}

void ResList::addResource(RESOURCE_PTR* resourcePtrPtr, const char* const resourcePath) {
	if(m_NumEntries < MAX_RES_LIST_ENTRIES) {
		ResListEntry* entry = m_Entries + m_NumEntries;
		entry->resourcePtrPtr = resourcePtrPtr;
		entry->resourcePath = resourcePath;
		++m_NumEntries;
	} else {
		ERROR("Too many resources in ResList!");
	}
}

//==============================================================================
// PathElementTable methods
//==============================================================================
PathElementTable::PathElementTable(uint32_t maxEntries) {
	m_InsertPtr = NULL;
	m_MaxEntries = 0;
	m_TableBuffer = MM_NEW char[maxEntries*ENTRY_SIZE];
	if(m_TableBuffer) {
		memset(m_TableBuffer, 0, maxEntries*ENTRY_SIZE);
		m_MaxEntries = maxEntries;
		m_InsertPtr = m_TableBuffer;
	}
}

PathElementTable::~PathElementTable() {
	if(m_TableBuffer) {
		MM_DELETE [] m_TableBuffer;
		m_TableBuffer = NULL;
	}
}

uint32_t PathElementTable::addElement(const char* const elementStr) {
	uint32_t result = INVALID_INDEX;
	
	if(m_TableBuffer) {
		// search for empty slot
		char* beyondEnd = m_TableBuffer + (m_MaxEntries*ENTRY_SIZE);
		while(*m_InsertPtr && m_InsertPtr < beyondEnd)
			m_InsertPtr += ENTRY_SIZE;
		
		// if we failed to find one, wrap to beginning and try again
		if(m_InsertPtr >= beyondEnd) {
			m_InsertPtr = m_TableBuffer;
			while(*m_InsertPtr && m_InsertPtr < beyondEnd)
				m_InsertPtr += ENTRY_SIZE;
		}

		if(!(*m_InsertPtr)) {
			// we've found an empty slot!
			strncpy(m_InsertPtr, elementStr, ENTRY_SIZE-1);
			result = (m_InsertPtr - m_TableBuffer) / ENTRY_SIZE;
			
			// set up search pointer to search from next slot
			m_InsertPtr += ENTRY_SIZE;
		}
	}
	
	return result;
}

uint32_t PathElementTable::findElement(const char* const elementStr) const {
	uint32_t result = INVALID_INDEX;
	if(m_TableBuffer) {
		char* beyondEnd = m_TableBuffer + (m_MaxEntries*ENTRY_SIZE);
		char* searchPtr = m_TableBuffer;
		while(searchPtr < beyondEnd && strncmp(searchPtr, elementStr, ENTRY_SIZE))
			searchPtr += ENTRY_SIZE;
		
		if (searchPtr < beyondEnd) {
			result = (searchPtr - m_TableBuffer) / ENTRY_SIZE;
		}
	}
	return result;
}

void PathElementTable::removeElement(uint32_t index) {
	if(m_TableBuffer && index < m_MaxEntries) {
		char* slotPtr = m_TableBuffer + (index*ENTRY_SIZE);
		memset(slotPtr, 0, ENTRY_SIZE);
	}
}

const char* PathElementTable::getElement(uint32_t index) const {
	const char* result = NULL;
	if(m_TableBuffer && index < m_MaxEntries) {
		result = m_TableBuffer + (index * ENTRY_SIZE);
	}
	return result;
}

//==============================================================================
// ResourcePathParser methods
//==============================================================================

ResourcePathParser::ResourcePathParser(const char* const resourcePath) {
	m_ResourcePath = resourcePath;
	m_ParsePtr = NULL;
	memset(m_CurrentElement, 0, MAX_ELEMENT_LENGTH);
	
	if (resourcePath) {
		// safely find end of resourcePath
		const char* beyondSanity = m_ResourcePath + MAX_PATH_LENGTH;
		m_ParsePtr = m_ResourcePath;
		while( *m_ParsePtr && (m_ParsePtr < beyondSanity))
			++m_ParsePtr;

		if (m_ParsePtr < beyondSanity) {
			// set up first element
			goToNextElement();
		} else {
			// signify that there are no elements to find
			m_ParsePtr = m_ResourcePath;
		}
	}
}

ResourcePathParser::~ResourcePathParser() {	
}

const char* const ResourcePathParser::getCurrElement() {
	return m_CurrentElement;
}

void ResourcePathParser::goToNextElement() {
	if(m_ParsePtr > m_ResourcePath) {
		// search for next element
		const char* beyondEnd = m_ParsePtr;
		while(	(m_ParsePtr > m_ResourcePath) &&
				(*m_ParsePtr != '\\') &&
				(*m_ParsePtr != '/')
			) {
			--m_ParsePtr;
		}
		
		// calculate how many characters are in current element
		// ...clipping if necessary
		int numChars = beyondEnd - m_ParsePtr;
		if(numChars > MAX_ELEMENT_LENGTH-1) {
			WARNING("ResourcePathParser: path element in \'%s\' too long.", 
					m_ResourcePath);
			numChars = MAX_ELEMENT_LENGTH-1;
		}
		
		// copy path element to this instance's buffer
		// ...and null-terminate it, just in case
		strncpy(m_CurrentElement, m_ParsePtr, numChars);
		m_CurrentElement[numChars] = '\0';
	} else {
		// blank out current element
		memset(m_CurrentElement, 0, MAX_ELEMENT_LENGTH);
	}
}
	
//==============================================================================
// ResManager methods
//==============================================================================
ResManager::ResManager(const char* baseResourcePath) {
	memset(m_ResourceMap, 0, sizeof(ResourceMapEntry)*MAX_RESOURCES);
	m_NumResources = 0;
	m_PathElementTable = MM_NEW PathElementTable(MAX_PATH_ELEMENT_ENTRIES);
	if(!m_PathElementTable) {
		ERROR("ResManager: Failed to allocate path element table.");
	}
}

ResManager::~ResManager() {
	if (m_PathElementTable) {
		MM_DELETE m_PathElementTable;
		m_PathElementTable = NULL;
	}
}

uint32_t ResManager::getOrCreateUUID(const char* const resourcePath) {
	ResourcePathParser pathParser(resourcePath);
	uint32_t result = 0;
	int32_t currShift = 24;
	
	while(!pathParser.isFinished() && currShift >= 0) {
		// check if path element is already in table
		uint32_t stringIdx = m_PathElementTable->findElement(
				pathParser.getCurrElement()
			);
		
		// if not...
		if (stringIdx == PathElementTable::INVALID_INDEX) {
			// ...add it to table
			stringIdx = m_PathElementTable->addElement(
					pathParser.getCurrElement()
				);
			
			// ...and double-check table isn't full!
			if (stringIdx == PathElementTable::INVALID_INDEX) {
				ERROR("ResManager: Failed to add \'%s\' to path element table",
						pathParser.getCurrElement()
					);
			}
		}
		
		// now add stringIdx into result
		result |= (stringIdx & 0xff) << currShift;
		
		// and move to next element
		currShift -= 8;
		pathParser.goToNextElement();
	}
	
	// if we failed to add any path elements, it's not a valid UUID
	if (result == 0) {
		result = INVALID_UUID;
	}
	
	return result;
}

uint32_t ResManager::getExistingUUID(const char* const resourcePath) {
	ResourcePathParser pathParser(resourcePath);
	uint32_t result = 0;
	int32_t currShift = 24;
	
	while(	!pathParser.isFinished() &&
			currShift >= 0 &&
			result != INVALID_UUID
		) {
			uint32_t stringIdx = m_PathElementTable->findElement(
					pathParser.getCurrElement()
				);
			
			if (stringIdx == PathElementTable::INVALID_INDEX) {
				result = INVALID_UUID;
			} else {
				result |= (stringIdx & 0xff) << currShift;
			}
			
			currShift -= 8;
			pathParser.goToNextElement();
	}
	
	return result;
}

// adds a list of resources to release on next change
void ResManager::setToRelease(const ResList* const resList) {
	
}

// adds a list of resources to load on next change
void ResManager::setToLoad(const ResList* const resList) {
	
}

// adds a list of resources to keep (i.e. override release) on next change
void ResManager::setToKeep(const ResList* const resList) {
	
}

// perform a previously specified resource change
void ResManager::doResourceChange() {
	
}

//==========================================================================
// Scene-level interface
//==========================================================================
const Bitmap* ResManager::getBitmap(uint16_t localID) {
	ERROR("Bitmap with localID %u not found.", localID);
	return NULL;
}

#if TEST
#include "testengine.h"

BEGIN_FIXTURE(ResourcePathParserTests)
	BEGIN_TEST(DummyTest)
		CHECK(true, "Dummy Test")
	END_TEST(DummyTest)
END_FIXTURE(ResourcePathParserTests)

#endif
