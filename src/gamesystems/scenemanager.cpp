/*
 * MIT License
 * 
 * Copyright (c) 2024 Greg King
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "scenemanager.h"

#include "gfxmanager.h"
#include "resmanager.h"
#include "timemanager.h"

SceneManager::SceneManager(GfxManager* gfx, ResManager* resMgr) {
	m_Gfx = gfx;
	m_ResMgr = resMgr;
	m_State = STATE_NONE;
	m_CurrScene = NULL;
	m_NextScene = NULL;
}

SceneManager::~SceneManager() {
	if (m_CurrScene) {
		MM_DELETE m_CurrScene;
		m_CurrScene = NULL;
	}
}

bool SceneManager::shouldQuit() {
	return (m_State == STATE_NONE) || (m_State == STATE_ERROR);
}

void SceneManager::startScene(Scene* (*sceneCreate)(SceneManager*)) {
	// set the state and next scene to be handled
	// in next update(...) call
	if (sceneCreate) {
		m_NextScene = (*sceneCreate)(this);
	}
	
	if(sceneCreate && m_NextScene) {
		m_State = STATE_SCENE_TRANSITION;
	} else {
		m_State = STATE_ERROR;
	}
}

void SceneManager::exit() {
	m_NextScene = NULL;
	m_State = STATE_SCENE_TRANSITION;
}

void SceneManager::update(uint32_t millis) {
	switch(m_State) {
		case STATE_ERROR:
		case STATE_NONE:
			break;
		case STATE_SCENE_TRANSITION:
			doSceneTransition();
			m_State = STATE_WAIT_FOR_FADE_IN;
			break;
		case STATE_WAIT_FOR_FADE_IN:
			if(!m_Gfx->isFading()) {
				m_State = STATE_SCENE_RUNNING;
			}
			break;
		case STATE_SCENE_RUNNING:
			if(m_CurrScene) {
				m_CurrScene->update(millis);
			} else {
				m_State = STATE_NONE;
			}
			break;
	}
}

void SceneManager::render() {
	if(m_CurrScene && m_State == STATE_SCENE_RUNNING) {
		m_CurrScene->render(m_Gfx->getRenderer());
		m_Gfx->flip();
	}
}


//==============================================================================
// Private methods
//==============================================================================

#define FADE_MILLIS (3000)

// Performs a scene transition...
// ...using interrupts to handle the graphics/audio fade
void SceneManager::doSceneTransition() {
	// if we have a current scene, we need to get it to cleanup and
	// tell us what resources to release
	if(m_CurrScene) {
		m_CurrScene->stop();
		ResList releaseList;
		m_CurrScene->registerPreLoadResources(&releaseList);
		m_CurrScene->registerLoadResources(&releaseList);
		m_ResMgr->setToRelease(&releaseList);
		
		MM_DELETE m_CurrScene;
		m_CurrScene = NULL;

		// trigger interrupt-driven fade out (i.e. non-blocking)
		m_Gfx->fadeToBlack(FADE_MILLIS);
		// fade out audio as well?
	}

	// if there is a next scene...
	// while fade is happening, we need to:
	//	- load the new scene's pre-load resources
	//	- make sure any already-loaded resources that will be required by the
	//	  new scene are not released
	//	(done this way so the resource manager may de-frag the
	//	 resource memory before the new scene gets any pointers)
	if(m_NextScene) {
		ResList preLoadList;
		m_NextScene->registerPreLoadResources(&preLoadList);
		m_ResMgr->setToLoad(&preLoadList);

		ResList loadList;
		m_NextScene->registerLoadResources(&loadList);
		m_ResMgr->setToKeep(&loadList);

		m_ResMgr->doResourceChange();

		// hopefully while fade-out is happening, but perhaps during solid
		// black screen, we need to:
		//	- get the new scene to render to the backbuffer
		//	  (making any required palette claims in the process)
		//	- kick off (or queue) an interrupt-driven buffer flip and fade-in
		//	  (non-blocking)
		m_NextScene->doPreloadRender(m_Gfx->getRenderer(), m_ResMgr);
		m_Gfx->flipFadeFromBlack(FADE_MILLIS, true);

		// while fade-in to initial screen is happening...
		//	- load any remaining resources required by the new scene
		//	- start the new scene
		m_ResMgr->setToLoad(&loadList);
		m_ResMgr->doResourceChange();

		m_NextScene->start();
	} else {
		// if there is no next scene, unload previous resources anyway
		m_ResMgr->doResourceChange();
	}
	
	// set m_CurrScene and blank out m_NextScene
	// (state change done in update(...) in the hope of better clarity)		
	m_CurrScene = m_NextScene;
	m_NextScene = NULL;
}
