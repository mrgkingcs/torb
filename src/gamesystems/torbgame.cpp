/*
 * MIT License
 * 
 * Copyright (c) 2024 Greg King
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "torbgame.h"

#include "gfxmanager.h"
#include "resmanager.h"
#include "scenemanager.h"
#include "scenes/teaserscene.h"

TorbGame::TorbGame() {
	m_Error = ERROR_NONE;
	m_GfxMgr = NULL;
	m_ResMgr = NULL;
	m_SceneMgr = NULL;
}

TorbGame::~TorbGame() {
	teardown();
}

// sets up all the game's systems ready to start ticking
void TorbGame::setup() {
	m_Error = ERROR_NONE;
	
	if (m_Error == ERROR_NONE) {
		m_GfxMgr = MM_NEW GfxManager();
		if (m_GfxMgr == NULL) {
			m_Error = ERROR_MEM_ALLOC_FAIL;
		}
	}
	
	if (m_Error == ERROR_NONE) {
		m_ResMgr = MM_NEW ResManager("assets");
		if (m_ResMgr == NULL) {
			m_Error = ERROR_MEM_ALLOC_FAIL;
		}
	}
	
	if (m_Error == ERROR_NONE) {
		m_SceneMgr = MM_NEW SceneManager(m_GfxMgr, m_ResMgr);
		if (m_SceneMgr == NULL) {
			m_Error = ERROR_MEM_ALLOC_FAIL;
		}
	}
	
	if (m_Error == ERROR_NONE) {
		m_GfxMgr->initMode13h();
		m_SceneMgr->startScene((*TeaserScene::create));
	}
}

// cleans up all the game's resources
void TorbGame::teardown() {
	if (m_GfxMgr) {
		m_GfxMgr->initMode3h();
		MM_DELETE m_GfxMgr;
		m_GfxMgr = NULL;
	}
	
	if (m_SceneMgr) {
		MM_DELETE m_SceneMgr;
		m_SceneMgr = NULL;
	}
	if (m_ResMgr) {
		MM_DELETE m_ResMgr;
		m_ResMgr = NULL;
	}
}

// updates and renders the game
void TorbGame::tick(uint32_t millis) {
	m_SceneMgr->update(millis);
	m_SceneMgr->render();
}

// returns true if main game loop should exit
bool TorbGame::getShouldQuit() {
	return m_SceneMgr->shouldQuit();
}

// returns the current error code of the game
uint32_t TorbGame::getError() {
	return m_Error;
}


