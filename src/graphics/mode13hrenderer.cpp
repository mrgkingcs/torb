/*
 * MIT License
 * 
 * Copyright (c) 2024 Greg King
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "mode13hrenderer.h"

#include <cstring>
using namespace std;

Mode13hRenderer::Mode13hRenderer() {
	m_Buffer = MM_NEW uint8_t[MODE_13h_BUFF_SIZE];
	if(m_Buffer) {
		memset(m_Buffer, 1, MODE_13h_BUFF_SIZE);
	}
}

Mode13hRenderer::~Mode13hRenderer() {
	if(m_Buffer) {
		MM_DELETE [] m_Buffer;
		m_Buffer = NULL;
	}
}

void Mode13hRenderer::clear(uint32_t colour) {
	if(m_Buffer) {
		memset(m_Buffer, (uint8_t)(colour & 0xff), MODE_13h_BUFF_SIZE);
	}
}

void Mode13hRenderer::drawBitmap(const Bitmap*, uint32_t left_x, uint32_t top_y) {
	
}

