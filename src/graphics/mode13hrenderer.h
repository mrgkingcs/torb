/*
  MIT License
  
  Copyright (c) 2024 Greg King
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this 
  software and associated documentation files (the "Software"), to deal in the Software 
  without restriction, including without limitation the rights to use, copy, modify, 
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
  permit persons to whom the Software is furnished to do so, subject to the following 
  conditions:
  
  The above copyright notice and this permission notice shall be included in all copies 
  or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef MODE13HRENDERER_H
#define MODE13HRENDERER_H

#include "core.h"
#include "gfxrenderer.h"

class Mode13hRenderer : public GfxRenderer {
private:
	static const uint32_t MODE_13h_BUFF_SIZE = (320*200*1);
	
	uint8_t* m_Buffer;
	
public:
	Mode13hRenderer();
	virtual ~Mode13hRenderer();
	
	virtual const uint8_t* const getBuffer() const { 
		return m_Buffer; 
	};
	virtual const uint32_t getBufferByteSize() const { 
		return MODE_13h_BUFF_SIZE; 
	}
	
	virtual void clear(uint32_t colour);
	virtual void drawBitmap(const Bitmap*, uint32_t left_x, uint32_t top_y);

};

#endif /* MODE13HRENDERER_H */

