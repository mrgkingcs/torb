/*
 * MIT License
 * 
 * Copyright (c) 2024 Greg King
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "timemanager.h"

uint32_t TimeManager::s_TickCount = 0;

_go32_dpmi_seginfo TimeManager::old_tick_handler;
_go32_dpmi_seginfo TimeManager::tick_handler;

TickCallback TimeManager::s_CallbackArr[TimeManager::MAX_CALLBACKS];
int TimeManager::s_NumCallbacks = 0;

void TimeManager::start() {
	INFO("setting tick freq to %uHz", TIME_ACTUAL_FREQ);
	
	// set timer mode 2
	outportb(0x43, 0b00110100);
	
	// set initial/reload value (low byte)
	outportb(0x40, TIME_FREQ_DIVIDER & 0xFF);
	
	// set initial/reload value (high byte)
	outportb(0x40, TIME_FREQ_DIVIDER >> 8);
	
	_go32_dpmi_lock_data(&s_TickCount, sizeof(s_TickCount));
	_go32_dpmi_lock_data(&s_CallbackArr, sizeof(s_TickCount));
	_go32_dpmi_lock_code((void*)tickHandler, 4096);
	
	_go32_dpmi_get_protected_mode_interrupt_vector(0x1c, &old_tick_handler);
	tick_handler.pm_offset = (int) tickHandler;
	tick_handler.pm_selector = _go32_my_cs();
	_go32_dpmi_allocate_iret_wrapper(&tick_handler);
	_go32_dpmi_set_protected_mode_interrupt_vector(0x1c, &tick_handler);
}

void TimeManager::stop() {
	_go32_dpmi_set_protected_mode_interrupt_vector(0x1c, &old_tick_handler);
}

void TimeManager::addTickCallback(TickCallback callback) {
	if(s_NumCallbacks < MAX_CALLBACKS) {
		s_CallbackArr[s_NumCallbacks] = callback;
		s_NumCallbacks++;
	}
}

#include <cstdio>
using namespace std;

void TimeManager::tickHandler() {
	s_TickCount++;
	for (int idx = 0; idx < s_NumCallbacks; idx++) {
		s_CallbackArr[idx]();
	}
}