/*
 * test.h
 * 
 * header for a simple unit test framework
 * 
 * author: Greg King
 * copyright (c) 2020
 * 
 * This code is distributed under the creative commons 
 * attribution non-commercial share-alike licence.
 * 
 * Licence text can be found here:
 * https://creativecommons.org/licenses/by-nc-sa/3.0/
 */

#ifndef __TEST_H__
#define __TEST_H__

#ifdef TEST

#include <cstdio>
#include <stdlib.h>
#include <string.h>

//#include "type.h"

using namespace std;

class TestCase;
class FixtureRunner;

void runTests();

//======================================================================
// singleton test runner to hold all the test cases
//======================================================================
class TestRunner {
private:
	static TestRunner* instance;
	FixtureRunner* fixtureHead;
	FixtureRunner* fixtureTail;
	TestRunner() {	fixtureHead = fixtureTail = NULL; }
	
	void runTestsInternal();
public:
	static TestRunner* getInstance() {
		if(instance == NULL) {
			instance = new TestRunner();
		}
		return instance;
	}
	
	static void destroyInstance() {
		if(instance != NULL) {
			delete instance;
			instance = NULL;
		}
	}
	
	void registerFixture(FixtureRunner* fixture);
		
	void runTests();
};

//======================================================================
// superclass for all test cases
//======================================================================
class TestCase {
public:
	TestCase* next;
	bool passed;

	TestCase();
	
	virtual void runTest() {}
	virtual const char* const getName() { return NULL; }
};

//======================================================================
// class to be hold multiple TestCases
//======================================================================
class FixtureRunner {
	TestCase *head, *tail;
	const char* name;
public:
	FixtureRunner* next;

	FixtureRunner();

	TestCase* getFirstTestCase() {	return head;	}
	
	void addTestCase(TestCase* newTestCase) {
		if(head == NULL) {
			head = tail = newTestCase;
		} else {
			tail->next = newTestCase;
			tail = newTestCase;
			newTestCase->next = NULL;
		}
	}
	
	void setName(const char* const _name);
	const char* const getName();
	
	void runTests();
	
	void printResults();
};


//======================================================================
// utility classes to simulate loading data from file
//======================================================================
//class memBuf : public streambuf {
//public:
//	memBuf(const byte *p, size_t l) {
//		setg((char*)p, (char*)p, (char*)p + l);
//	}
//	int overflow(int a) { return 0; }
//	int underflow(void) { return EOF; }
//};
//
//class memStream : public istream {
//public:
//	memStream(const byte *p, size_t l) :
//		std::istream(&_buffer),
//		_buffer(p, l) {
//	}
//
//private:
//	memBuf _buffer;
//};


//======================================================================
// base64.c declarations
//======================================================================
unsigned char * base64_decode(const unsigned char *src, size_t len,
			      size_t *out_len);


//======================================================================
// Unit test macros
//======================================================================
#define BEGIN_FIXTURE(FIXTURE_NAME) \
class FIXTURE_NAME { \
public: \
	static const char* const name; \
	static FixtureRunner fixtureRunner; \
	FIXTURE_NAME() { \
		fixtureRunner.setName(name); \
		TestRunner::getInstance()->registerFixture(&fixtureRunner); \
	}		

#define END_FIXTURE(FIXTURE_NAME) \
}; \
FIXTURE_NAME FIXTURE_NAME; \
FixtureRunner FIXTURE_NAME::fixtureRunner; \
const char* const FIXTURE_NAME::name = #FIXTURE_NAME;

#define BEGIN_TEST(TEST_NAME) \
    class TEST_NAME : public TestCase { \
		public: \
			TEST_NAME() : TestCase() { fixtureRunner.addTestCase(this); } \
			const char* const getName() { return #TEST_NAME; } \
			void runTest() {

#define END_TEST(TEST_NAME) \
				passed = true; \
			} \
    }; \
    TEST_NAME TEST_NAME;

#define CHECK(condition, message) \
	if(!(condition)) { \
		passed = false; \
		printf( "\t\tFAIL: %s\n", (message)); \
		return; \
	}

#endif

#endif
