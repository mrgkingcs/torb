/*
  MIT License
  
  Copyright (c) 2024 Greg King
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this 
  software and associated documentation files (the "Software"), to deal in the Software 
  without restriction, including without limitation the rights to use, copy, modify, 
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
  permit persons to whom the Software is furnished to do so, subject to the following 
  conditions:
  
  The above copyright notice and this permission notice shall be included in all copies 
  or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef TEASERSCENE_H
#define TEASERSCENE_H

#include "gamesystems/scenemanager.h"

class Bitmap;

class TeaserScene : public Scene {
private:
	TeaserScene(SceneManager* manager);
	
	int32_t m_StateTimer;
	
	// resource handles
	Bitmap* m_TitleLogoPtr;
	
public:
	virtual ~TeaserScene();
	
	static Scene* create(SceneManager* manager);

	void registerPreLoadResources(ResList* resList) const;
	void registerLoadResources(ResList* resList) const;
	
	void start();
	void stop();
	
	void doPreloadRender(GfxRenderer* gfx, ResManager* res);
	
	void update(uint32_t millis);
	void render(GfxRenderer* gfx);
};

#endif /* TEASERSCENE_H */

