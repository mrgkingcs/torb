/*
 * MIT License
 * 
 * Copyright (c) 2024 Greg King
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "teaserscene.h"

#include "core.h"
#include "gamesystems/resmanager.h"
#include "graphics/gfxrenderer.h"

TeaserScene::TeaserScene(SceneManager* manager)
	: Scene::Scene(manager)
{
}

TeaserScene::~TeaserScene() {
	
}

// static factory method for SceneManager
Scene* TeaserScene::create(SceneManager* manager) {
	return MM_NEW TeaserScene(manager);
}

// Register any resources that are required for doPreloadRender()
void TeaserScene::registerPreLoadResources(ResList* resList) const {
	resList->addResource((RESOURCE_PTR*)&m_TitleLogoPtr, "teaser\\title.res");
}

// Register any resources for the scene that are 
// _not_ required for doPreloadRender()
void TeaserScene::registerLoadResources(ResList* resList) const {
	
}	

void TeaserScene::start() {
	// set up a dum 3 second timer
	m_StateTimer = 3*1000;
}

void TeaserScene::stop() {
	
}

void TeaserScene::doPreloadRender(GfxRenderer* gfx, ResManager* res) {
	gfx->drawBitmap(m_TitleLogoPtr, 0, 0);
}

void TeaserScene::update(uint32_t millis) {
	// for now, just quit the game after the timer croaks
	if (m_StateTimer > millis) {
		m_StateTimer -= millis;
	} else {
		m_StateTimer = 0;
		getSceneManager()->exit();
	}
}

void TeaserScene::render(GfxRenderer* gfx) {
	
}

