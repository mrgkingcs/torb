# ASM integration

As per [djgpp docs](https://www.delorie.com/djgpp/doc/ug/asm/calling.html):

## ASM routine MUST preserve:

- ebx
- esi
- edi
- ebp
- ds
- es
- ss

## Parameters (on stack):

(%esp) --> return address
4(%esp) --> first argument
...
...

## Return values:

- int (<=32bits) and pointers in eax
- 64-bit ints (long long int) in edx:eax (edx = most-significant)
- void: contents of these registers are not used

## Common stack preservation:

- pushl %ebp
- movl %esp --> %ebp
- ...
- (can use offset-addressing from %ebp to access arguments)
- ...
- popl %ebp


## Example:

    # Stack layout on entry:
    #
    #          (high half of b)
    # 12(%esp) b
    #          (high half of a)
    # 4(%esp)  a
    # (%esp)   return address
    
    .globl _ull_avg
    _ull_avg:
            movl 4(%esp), %eax
    	movl 8(%esp), %edx
    	addl 12(%esp), %eax    # Add low halves
    	adcl 16(%esp), %edx    # Add high halves, with carry
    	shrdl $1, %edx, %eax
    	shrl $1, %edx	       # Divide by 2
    	ret		       # Return value is in %edx:%eax
    


Or, in C:

    unsigned long long ull_avg (unsigned long long a, unsigned long long b)
    {
      return (a + b) / 2;
    }

